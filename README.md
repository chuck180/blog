## TxGroup Reactjs Coding Challenge

I enjoyed building this small side project (:

    # Code could be improved with:
    Redux, global store state management instead of state management in components
    Test Coverage / More tests
    Typescript for strong typing
    next.js for server side rendering

### Updated TypeScript Version of the initial project from 2020

- [x] Removed all outdated node packages with over 100 critical vunerablities.
- [ ] **TODO:** App is not translated to all the types and fileformats tsx, there are still old js files.


### To run the new Vite project
```
git clone git@gitlab.com:chuck180/blog.git
npm install
npm run dev
```

![Alt text](src/assets/img/mockapiexampleblog.png)
![Alt text](src/assets/img/mockapiexampleblog2.png)