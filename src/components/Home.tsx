import logo from "../assets/img/600px-Logo_tx_group.png"


function Home() {
    return (
        <>
            <div className="d-flex flex-column mb-3">
                <div className="p-2 d-flex justify-content-center">
                    <img height="300"
                        src={logo}
                        alt="tx group logo"/>
                </div>
                <div className="p-2 d-flex justify-content-center">
                    <h1 className="text-secondary">
                        Oliver Willhalm
                    </h1>
                </div>
                <div className="p-2 d-flex justify-content-center">
                    <h1 className="text-white">
                        TX Group Coding Challenge
                    </h1>
                </div>
            </div>
        </>
        
    )
}

export default Home;
