import { useState } from "react";
import { Link } from "react-router-dom";

import abstract_logo from "../../assets/img/tx_group.jpg";


function Nav() {

    const [isOpen, setToOpen] = useState("")

    const openMobileMenu = () => {
        const node = document.querySelector("#mobileMenu");
        !node?.classList.contains("show") ? setToOpen("show") : setToOpen("")
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light mb-3"
             data-testid={`navbar`}>
            <span className="navbar-brand">
                <Link to="/">
                    <img src={abstract_logo}
                         width="30"
                         height="30"
                         alt="tx group abstract logo"
                         loading="lazy" />
                </Link>
            </span>
            <button className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#mobileMenu"
                    aria-controls="mobileMenu"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                <span onClick={() => openMobileMenu()}
                      className="navbar-toggler-icon">
                </span>
            </button>
            <div className={`collapse navbar-collapse ${isOpen}`}
                 id="mobileMenu">
                <div className="navbar-nav">
                    <span className="nav-link active">
                        <Link to="/posts">
                            Posts
                        </Link>
                    </span>
                </div>
            </div>
        </nav>
    )
}

export default Nav;
