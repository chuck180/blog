import React from "react";


function BootstrapAlert(props) {
    return (<div className={`d-flex w-100 alert alert-${props.bootstrapColorClass}`} role="alert">
        {props.alertMessage}
    </div>)
}

export default BootstrapAlert;