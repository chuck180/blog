


const Pluralize = ({count}:any) => {
    switch (count) {
        case 1:
            return `${count} Kommentar`;
        default:
            return `${count} Kommentare`;
    }
}

export default Pluralize;