
type LoadingSpinnerType = {
    bootstrapSizeClass: string,
    bootstrapTextColorClass: string
}

const LoadingSpinner = (
    loadingSpinner: LoadingSpinnerType
): JSX.Element => {
    return (<div className={`spinner-border ${loadingSpinner.bootstrapSizeClass} ${loadingSpinner.bootstrapTextColorClass}`}
                 role="status">
    </div>)
}

export default LoadingSpinner;