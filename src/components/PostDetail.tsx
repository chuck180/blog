import { useEffect, useState } from "react";
import moment from "moment-timezone";
import { Link } from "react-router-dom";

//import LoadingSpinner from "./layout/LoadingSpinner";
//import BootstrapAlert from "./layout/BootstrapAlert";

const formatDate = (timestamp: any) => {
    const newYork = moment(timestamp * 1000).tz("America/New_York");
    return newYork.clone().tz("Europe/Amsterdam").format("LLL");
};


function PostDetail({ match }: any) {

    useEffect(() => {
        fetchComments();
        fetchPost();
    },[]);

    const [comments, setComments] = useState<any>([]);
    const [post, setPost] = useState<any>({});
    const [isLoading, setIsLoading] = useState(true);
    const [formHasError, setFormHasError] = useState(false);
    const [formHasValidData, setFormHasValidData] = useState(false);
    const [fromHasBeenSubmitted, setFromHasBeenSubmitted] = useState(false);

    const fetchComments = async () => {
        const result = await fetch(`https://5ebd9842ec34e900161923e7.mockapi.io/post/${match.params.id}/comments`);
        const comments = await result.json();
        setComments(comments);
    }

    const fetchPost = async () => {
        setIsLoading(true);
        const result = await fetch(`https://5ebd9842ec34e900161923e7.mockapi.io/post/${match.params.id}`);
        if (result.status >=  200 && result.status <= 299) {
            const postBlock = await result.json();
            setPost(postBlock);
            setIsLoading(false);
        } else if (result.status === 429) {
            setTimeout(() => {fetchPost()}, 200)
        }
    }

    const handleTextAreaInput = (event: React.FormEvent<HTMLInputElement>) => {
        setFormHasValidData(false);

        setNewComment((event.target as HTMLInputElement).value);
        if ((event.target as HTMLInputElement).value.length > 0) {
            setFormHasError(false);
            setFormHasValidData(true);
        } else {
            setFormHasError(true);
        }
    }

    const [authorName, setAuthorName] = useState("");
    const [newComment, setNewComment] = useState("");

    const handleForm = async () => {
        if (formHasValidData) {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    postId: match.params.id,
                    name: authorName,
                    text: newComment,
                })
            };
            const response = await fetch(`https://5ebd9842ec34e900161923e7.mockapi.io/post/${match.params.id}/comments`, requestOptions);
            const result = await response.json();
            setComments([...comments, result]);
            setFromHasBeenSubmitted(true);
            setAuthorName("");
            setNewComment("");
        } else {
            setFromHasBeenSubmitted(false);
        }
    }

    return (
        <div className="jumbotron bg-secondary">
            <h4>
                post
            </h4>
            <hr />

            <div className="card w-100 mb-5">
                <div className="card-body">
                    <h5 className="card-title">{post.title}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">{post.description}</h6>
                    <p className="card-text">{post.text}.</p>
                </div>
                <div className="card-footer small">
                    <div className="d-flex justify-content-center">
                        posted at: {formatDate(post.createdAt)}
                    </div>
                </div>
            </div>

            {!isLoading ? (
                <div>
                    {comments.length > 0 ? (
                        <Comments comments={comments} />
                    ) : ""}
                </div>
            ) : (
                `loadingspinner`
                //<div className="d-flex justify-content-center">
                    //<LoadingSpinner bootstrapSizeClass={`spinner-border-sm`} bootstrapTextColorClass={`text-white`} />
                //</div>
            )}

            <div className="my-5">
                <h4>
                    Leave a comment
                </h4>
                <hr />

                {formHasError ? (
                    `bootstrap alert`
                    //<BootstrapAlert alertMessage={`Error, the Form needs content to be submitted!`} bootstrapColorClass={`danger`} />
                ) : (
                    <>
                    </>
                )}

                {formHasValidData && !fromHasBeenSubmitted ? (
                    `bootstrap alert`
                    //<BootstrapAlert alertMessage={`Form is valid!`} bootstrapColorClass={`success`} />
                ) : (
                    <>
                    </>
                )}

                {fromHasBeenSubmitted ? (
                    `bootstrap alert`
                    //<BootstrapAlert alertMessage={`Your comment has been submitted!`} bootstrapColorClass={`success`} />
                ) : (
                    <>
                    </>
                )}

                <form method="POST">
                    <div className="form-group">
                        <label htmlFor="authorName">
                            author
                        </label>
                        <input onChange={(event) => setAuthorName(event.target.value)}
                               value={authorName}
                               required={false}
                               type="text"
                               className="form-control"
                               id="authorName" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="newComment">
                            comment
                        </label>
                        <textarea onChange={(event: any) => handleTextAreaInput(event)}
                                  value={newComment}
                                  className="form-control"
                                  id="newComment"
                                  rows={4} />
                    </div>

                    <div className="d-flex justify-content-between">
                        <div>
                            <button className="btn btn-primary">
                                <Link to={`/posts`}>
                                <span className="text-white">
                                    back
                                </span>
                                </Link>
                            </button>
                        </div>
                        <div>
                            <button onClick={() => handleForm()}
                                    type="button"
                                    className="btn btn-success">
                                submit comment
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    )
}

const Comments = (props: any) => {
    return (
        <div>
            <h4>comments</h4>
            <hr/>
            {props.comments.map((comment: any) => (
                <div key={comment.id} className="card w-100 my-3">
                    <div className="card-body">
                        <p className="card-text">{comment.text}.</p>
                    </div>
                    <div className="card-footer small d-flex justify-content-between">
                        <div>
                            author: {comment.name}
                        </div>
                        <div>
                            posted at: {formatDate(comment.createdAt)}
                        </div>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default PostDetail;
