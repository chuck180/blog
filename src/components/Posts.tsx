import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import LoadingSpinner from "./layout/LoadingSpinner";
import Pluralize from "./layout/Pluralize";

type PostType = {
    id: string,
    createdAt: number,
    title: string,
    description: string
};

function Posts() {

    const pageLength = 5;
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [posts, setPosts] = useState<PostType[] | undefined>();
    const [stored, setStored] = useState<PostType[] | undefined>([]);
    const [postsBlock, setPostsBlock] = useState<PostType[] | undefined>()
    const [pageIndex, setPageIndex] = useState<number>(1);

    useEffect(() => {
        fetchPosts();
    },[pageIndex])

    function checkStatus(response: any) {
        setIsLoading(true);
        if (response.ok) {
            return Promise.resolve(response);
        } else {
            setIsLoading(false);
            return Promise.reject(new Error(response.statusText));
        }
    }

    function parseJSON(response: any) {
        setIsLoading(false);
        return response.json();
    }

    const fetchPosts = async () => {
        setIsLoading(true);
        const result = await fetch(`https://5ebd9842ec34e900161923e7.mockapi.io/post?page=${pageIndex}&limit=${pageLength}`)
            .then(checkStatus)
            .then(parseJSON)
            .catch(error => console.log('There was a problem in fetching Posts!', error))
            .then((ps: any) => {
                // TODO cleanup fetching posts in to stored arraylist works now [5posts + 5posts + 5posts]
                // cant do alot requests, 429 due to free api restriction, Too many requests 429
                console.log(ps + "ps")
                console.log(stored + "ps")
                setStored(stored?.concat(ps))
                console.log(stored?.concat(ps) + "concat")
                console.log(stored + "stored")
                setIsLoading(false);
            })
    }

    return (
        <div>
            <ul className="list-group mb-3">
                {stored?.map((p: Post): JSX.Element => (
                    <li key={p?.id}
                        className="list-group-item d-flex justify-content-between">
                        <div>
                            <Link to={`/posts/${p?.id}`}>
                                {p.title}, {p.description}
                            </Link>
                        </div>
                        <div>
                            <CommentCount id={p.id} createdAt={p.createdAt} title={p.title} description={p.description} />
                        </div>
                    </li>
                ))}
            </ul>

            <div className="d-flex justify-content-center my-4">
                {!posts && isLoading ? (
                    <LoadingSpinner bootstrapSizeClass={`spinner-border`} bootstrapTextColorClass={`text-danger`} />
                ) : 'comments loaded'}
            </div>

            <button onClick={() => setPageIndex(pageIndex + 1)} className="btn btn-primary w-100">Load more..</button>
        </div>
    )
}

const CommentCount = (post: Post): React.JSX.Element => {

    const [count, setCount] = useState<number>(-1);

    useEffect(() => {
        fetchComments();
    },[count])

    const fetchComments = async () => {
        if (typeof post !== "undefined") {
            const result = await fetch(`https://5ebd9842ec34e900161923e7.mockapi.io/post/${post.id}/comments`, {
                method: 'get',
            }).then((response: Response) => {
                if(!response.ok) {
                    //error
                    throw Error;
                }
                return response.json()
            });
            const comments = result;
            setCount(comments.length);
            // if (result.status >=  200 && result.status <= 299) {
            //      const comments = result;
            //      console.log(comments);
            //      setCount(comments.length);
            // } else if (result.status === 429) {
            //      setTimeout(() => {fetchComments()}, 10000)
            // }
        }
    }

    return (
        <div>
            {count < 0 ? (
                <LoadingSpinner bootstrapSizeClass={`spinner-border`} bootstrapTextColorClass={`text-danger`} />
            ) : <Pluralize count={count} />
        }
            
        </div>
    )
}

export default Posts;
