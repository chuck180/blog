import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Comments from "../components/PostDetail"


afterEach(cleanup);

it("renders individual comments for a blog post", () => {

    const comments = {
        comments: [
            {
                id: 1,
                name: "First Comment Title",
                text: "First Comment Text",
                createdAt: 1605545827,
            },
            {
                id: 2,
                name: "Second Comment Title",
                text: "Second Comment Text",
                createdAt: 1605545827,
            }
        ]
    }

    const display = async () => {
        render(<Comments comments={comments} />, { wrapper: MemoryRouter });
        expect(await screen.getByText("/First Comment Title/")).toBeInTheDocument();
        expect(await screen.getByText("/First Comment Text/")).toBeInTheDocument();
        expect(await screen.getByText("/Second Comment Title/")).toBeInTheDocument();
        expect(await screen.getByText("/Second Comment Text/")).toBeInTheDocument();
    }
});
