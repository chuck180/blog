import React from "react";
import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import App from "../App";


afterEach(cleanup);

it("renders app with content", () => {
    render(<App />);
    expect(screen.getByText("Oliver Willhalm")).toBeInTheDocument();
    expect(screen.getByText("TX Group Coding Challenge")).toBeInTheDocument();
});

it("test routing to /posts", function () {
    render(<App />, { wrapper: MemoryRouter });
    expect(screen.getByText("Oliver Willhalm")).toBeInTheDocument();

    const leftClick = {Button: 0}
    fireEvent.click(screen.getByText("Posts"), leftClick);
    screen.getByText("Load more..");
});