import React from "react";
import { cleanup, render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Nav from "../components/layout/Nav";


afterEach(cleanup);

it("render nav with test id", () => {
    const {getByTestId} = render(<Nav />, { wrapper: MemoryRouter })
    expect(getByTestId("navbar")).toBeTruthy();
});