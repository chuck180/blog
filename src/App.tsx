import reactLogo from './assets/react.svg'
import viteLogo from '../public/vite.svg'
import './App.css'

import {
  BrowserRouter as Router,
  Route,
  Routes
} from 'react-router-dom'

import Nav from './components/layout/Nav'
import Home from './components/Home'
import Posts from './components/Posts'
import PostDetail from './components/PostDetail'

function App() {
  return (
    <>
      <Router>
        <Nav />
        <div className="container pt-4">
          <Routes>
            <Route path={"/"} Component={Home} />
            <Route path={"/posts"} Component={Posts} />
            <Route path={"/posts/:id"} Component={PostDetail} />
          </Routes>
        </div>
      </Router>
      <div className='d-flex justify-content-center text-center'>
        <div className='d-flex flex-column'>
          <div className="p-2">
            <a href="https://vitejs.dev" target="_blank">
              <img src={viteLogo} className="logo" alt="Vite logo" />
            </a>
            <a href="https://react.dev" target="_blank">
              <img src={reactLogo} className="logo react" alt="React logo" />
            </a>
          </div>
          <div className="p-2">
            <h1>Vite + React</h1>
            <p className="read-the-docs">
              Click on the Vite and React logos to learn more
            </p>
          </div>
        </div>
      </div>
    </>
  )
}

export default App
